<?php
include_once("CreateZipFile.inc.php");
$createZipFile=new CreateZipFile;
 
$directoryToZip="var/export"; // This will zip all the file(s) in this present working directory. Change this to your export directory
 
$outputDir="/"; //Replace "/" with the name of the desired output directory.
$zipName="export.zip";
 
//Code toZip a directory and all its files/subdirectories
$createZipFile->zipDirectory($directoryToZip,$outputDir);
 
$rand=time();
$zipName=$rand."_".$zipName;
$fd=fopen($zipName, "wb");
$out=fwrite($fd,$createZipFile->getZippedfile());
fclose($fd);
$createZipFile->forceDownload($zipName);
@unlink($zipName);
 
?>