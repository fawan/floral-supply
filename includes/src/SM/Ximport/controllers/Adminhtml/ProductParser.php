<?php

class ProductsParser {

    public function parseCSVCategories($File) {

        $products = array();
        $handle = fopen($File, "r");


        $fields = fgetcsv($handle, 0, ",");


        while ($data = fgetcsv($handle, 0, ",")) {
            $detail[] = $data;
        }

        $x = 0;
        $y = 0;

        foreach ($detail as $i) {
            foreach ($fields as $z) {
                //echo '<br>' . $z . ' : ' . $x . ' : ' . $y;			
                if ($y < 80)
                    $products[$x][$z] = $i[$y];
                $y++;
            }
            $y = 0;
            $x++;
        }

        return $products;
    }

}
