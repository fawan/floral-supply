<?php

require_once 'ProductParser.php';

class SM_Ximport_Adminhtml_XimportController extends Mage_Adminhtml_Controller_action {

    protected $_categoryCount = 0;
    protected $_productCount = 0;

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('ximport/items')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('ximport/ximport')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('ximport_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('ximport/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('ximport/adminhtml_ximport_edit'))
                    ->_addLeft($this->getLayout()->createBlock('ximport/adminhtml_ximport_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ximport')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function exportAction() {


        $this->loadLayout();
        $this->_setActiveMenu('ximport/export');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('ximport/adminhtml_ximport_export'))
                ->_addLeft($this->getLayout()->createBlock('ximport/adminhtml_ximport_export_tabs'))
        ;

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function exportProductsAction() {

        try {
            $model = Mage::getModel('ximport/xexport');
            $model->setData($this->getRequest()->getParams());

            return $this->_prepareDownloadResponse(
                            $model->getFileName(), $model->export(), 'text/csv'
            );
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }


        return $this->_redirect('*/*/index');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            $stores = $data['stores'];
            $importModel = Mage::getModel('ximport/ximport');
            try {
                Mage::log("import Started...", null, "ximport.log");
                $filePath = $importModel->uploadSource();
                $this->importCSV($filePath, $stores);
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ximport')->__('Total ' . $this->_productCount . ' products was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                Mage::log("import complete...", null, "ximport.log");
                $this->_forward('edit');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                Mage::log($e->getMessage());
                $this->_forward('edit');
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ximport')->__('Unable to find item to import'));
        $this->_redirect(' */*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('ximport/ximport');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/* /');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect(' */*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/* /');
    }

    public function massDeleteAction() {
        $ximportIds = $this->getRequest()->getParam('ximport');
        if (!is_array($ximportIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($ximportIds as $ximportId) {
                    $ximport = Mage::getModel('ximport/ximport')->load($ximportId);
                    $ximport->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($ximportIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect(' */*/index');
    }

    public function massStatusAction() {
        $ximportIds = $this->getRequest()->getParam('ximport');
        if (!is_array($ximportIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($ximportIds as $ximportId) {
                    $ximport = Mage::getSingleton('ximport/ximport')
                            ->load($ximportId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($ximportIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect(' * /* /index');
    }

    public function exportCsvAction() {
        $fileName = 'ximport.csv';
        $content = $this->getLayout()->createBlock('ximport/adminhtml_ximport_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = 'ximport.xml';
        $content = $this->getLayout()->createBlock('ximport/adminhtml_ximport_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    protected function _parseXML($filePath) {

        $xmlObj = new Varien_Simplexml_Config($filePath);
        $xmlData = $xmlObj->getNode('global');
    }

    public function updateProductAction() {
        $categoryObj = Mage::getModel('ximport/category');
        for ($i = 1; $i <= 38588; $i++) {
            if ($i != 3320) {
                $categoryObj->addProductAttribute($i);
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ximport')->__('Total 38588 products was successfully saved'));
        $this->_forward('edit');
    }

    public function importCSV($filePath, $storeIds) {

        $csvHandler = new ProductsParser();
        $catalogObj = Mage::getModel('ximport/category');
        $catalogObj->setStoreId($storeIds);
        $importOptions = $this->getRequest()->getParam('import_options');
        $catalogObj->setImportCustomOptions($importOptions);
        $productsArray = $csvHandler->parseCSVCategories($filePath);
        $catalogObj->importProduct($productsArray);
        $this->_productCount = count($productsArray);
        unset($productsArray);

        $indexes = array(1, 2, 8);

        foreach ($indexes as $i) {
            $process = Mage::getModel('index/process')->load($i);
            $process->reindexAll();
        }
    }

}
