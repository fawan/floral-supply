<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2014 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_OrderBy {
	public function toOptionArray() {
		return array(
            array('value' => 'name',          'label' => Mage::helper('edge')->__('Name')),
            array('value' => 'price',         'label' => Mage::helper('edge')->__('Price')),
            array('value' => 'created_at',    'label' => Mage::helper('edge')->__('Date Created')),
            array('value' => 'random',        'label' => Mage::helper('edge')->__('RANDOM')),
            array('value' => 'top_rating',    'label' => Mage::helper('edge')->__('Top Rating')),
            array('value' => 'top_reviews',   'label' => Mage::helper('edge')->__('Top Reviews')),
            array('value' => 'top_views',     'label' => Mage::helper('edge')->__('Top Views')),
            array('value' => 'top_sales',     'label' => Mage::helper('edge')->__('Top Selling')),
        );
	}
}
