<?php

class SM_Ximport_Block_Adminhtml_Ximport_Export_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('ximport_form', array('legend' => Mage::helper('ximport')->__('Export Settings')));



        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => Mage::helper('ximport')->__('Store View'),
                'title' => Mage::helper('ximport')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        } else {

            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }
     
        $fieldset->addField('sku', 'text', array(
            'label' => Mage::helper('ximport')->__('SKU Prefix'),
            'required' => false,
            'name' => 'sku',
            'after_element_html' => $this->__('Vendor SKU prefix:  Leave empty to export all products')
        ));
        
        if (Mage::getSingleton('adminhtml/session')->getXimportData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getXimportData());
            Mage::getSingleton('adminhtml/session')->setXimportData(null);
        } elseif (Mage::registry('ximport_data')) {
            $form->setValues(Mage::registry('ximport_data')->getData());
        }
        return parent::_prepareForm();
    }

}
