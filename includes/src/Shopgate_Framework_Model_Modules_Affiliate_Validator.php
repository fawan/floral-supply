<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */
class Shopgate_Framework_Model_Modules_Affiliate_Validator extends Shopgate_Framework_Model_Modules_Validator
{
    /**
     * The parameters that are in the 'tracking_get_parameters' array
     *
     * @var array
     */
    private $affiliateParameters;

    /**
     * Holds the default parameters and on init
     * get populated with custom parameters that
     * may be defined in the system > config
     *
     * @var array
     */
    protected $validParams = array();

    /**
     * Checks if the script ran already
     *
     * @var bool
     */
    protected $scriptRan = false;

    /**
     * Affiliate params are passed into constructor
     */
    public function _construct()
    {
        $this->affiliateParameters = current($this->_data);
        $this->assignParamsToKeys();
        $this->removeUnassignedKeys();
    }

    /**
     * Returns all valid parameters
     * of the module
     *
     * @return array
     */
    public function getValidParams()
    {
        return $this->validParams;
    }

    /**
     * Checks if the current route is valid
     * based on the parameter passed
     *
     * @return bool
     */
    public function checkGenericValid()
    {
        $validParams = $this->getValidParams();

        foreach ($this->affiliateParameters as $param) {
            if (isset($param['key']) && isset($validParams[$param['key']])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Assign passed params to keys
     */
    private function assignParamsToKeys()
    {
        foreach ($this->affiliateParameters as $param) {
            if ($this->isKeySetButNoValue($param)) {
                $this->validParams[$param['key']] = $param['value'];
            } elseif (isset($this->validParams[$param['key']])) {
                unset ($this->validParams[$param['key']]);
            }
        }
    }

    /**
     * Initializes the valid params and checks if
     * the key exists for the passed parameter
     *
     * @param array $param - e.g. array('key'=> 'key', 'value' => 'value')
     *
     * @return bool
     */
    private function isKeySetButNoValue($param)
    {
        $validParams = $this->getValidParams();

        return isset($param['key']) && isset($validParams[$param['key']]) && $validParams[$param['key']] === false;
    }

    /**
     * Initializes removal of unused parameter keys
     */
    private function removeUnassignedKeys()
    {
        foreach ($this->validParams as $key => $param) {
            if ($this->validParams[$key] === false || $this->validParams[$key] === '') {
                unset($this->validParams[$key]);
            }
        }
    }
}
