<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */
class Shopgate_Framework_Model_Modules_Affiliate_Router implements Shopgate_Framework_Model_Interfaces_Modules_Router
{
    const VALIDATOR        = 'validator';
    const REDEEM           = 'redeem';
    const CLASS_SHORT_PATH = 'shopgate/modules_affiliate_packages';

    /** @var ShopgateOrder | ShopgateCart */
    private $sgOrder;
    /** @var string $directoryName */
    private $directoryName;

    /**
     * @param array $data - should contain an array of affiliate params in first element
     *
     * @throws Exception
     */
    public function __construct(array $data)
    {
        $sgOrder = current($data);
        if (!$sgOrder instanceof ShopgateCartBase) {
            $error = Mage::helper('shopgate')->__('Incorrect class provided to: %s::_constructor()', get_class($this));
            ShopgateLogger::getInstance()->log($error);
            throw new Exception($error);
        }
        $this->sgOrder = $sgOrder;
    }

    /**
     * @return Shopgate_Framework_Model_Modules_Affiliate_Validator
     */
    public function getValidator()
    {
        $trackingParams = $this->sgOrder->getTrackingGetParameters();
        $validator      = $this->getPluginModel(self::VALIDATOR, array($trackingParams));

        return $validator ? $validator : Mage::getModel('shopgate/modules_validator', array($trackingParams));
    }

    /**
     * Retrieves the redeemer class
     *
     * @return false | Shopgate_Framework_Model_Modules_Affiliate_Packages_Magestore_Redeem
     */
    public function getRedeemer()
    {
        return $this->getPluginModel(self::REDEEM);
    }

    /**
     * Name of the package directory
     *
     * @param string $name
     *
     * @return $this
     */
    public function setDirectoryName($name)
    {
        $this->directoryName = $name;

        return $this;
    }

    /**
     * Retrieve the directory name
     *
     * @return string
     */
    public function getDirectoryName()
    {
        return $this->directoryName;
    }

    /**
     * Retrieves a model to access
     *
     * @param string $path
     * @param array  $params
     *
     * @return mixed
     */
    private function getPluginModel($path, $params = array())
    {
        return $this->initAffiliateClass($this->getDirectoryName(), $path, $params);
    }

    /**
     * Small helper that concatenate the first two params given
     * with an underscore & loads the model
     *
     * @param string $partOne - first part of class name
     * @param string $partTwo - second part of class name
     * @param array  $data    - constructor params
     *
     * @return mixed
     */
    private function initAffiliateClass($partOne, $partTwo, $data = array())
    {
        $partOne = self::CLASS_SHORT_PATH . '_' . lcfirst($partOne);

        return @ Mage::getModel($partOne . '_' . lcfirst($partTwo), $data);
    }
}
