<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author             Shopgate GmbH <interfaces@shopgate.com>
 * @coversDefaultClass Shopgate_Framework_Model_Modules_Affiliate_Packages_KProject_Validator
 * @group              Shopgate_Modules_Affiliate
 */
class Shopgate_Framework_Test_Model_Modules_Affiliate_Packages_KProject_ValidatorTest
    extends Shopgate_Framework_Test_Model_Utility
{

    public function setUp()
    {
        $this->getModuleConfig(Shopgate_Framework_Model_Modules_Affiliate_Packages_KProject_Validator::MODULE_CONFIG);
    }

    /**
     * @param array $expected
     * @param array $params
     *
     * @covers ::getValidParams
     * @covers ::assignParamsToKeys
     * @covers ::isKeySetButNoValue
     * @covers ::removeUnassignedKeys
     *
     * @dataProvider getValidParamsProvider
     */
    public function testGetValidParams($expected, $params)
    {
        $class  = Mage::getModel('shopgate/modules_affiliate_packages_kProject_validator', array($params));
        $return = $class->getValidParams();

        $this->assertEquals($expected, $return);
    }

    /**
     * @return array
     */
    public function getValidParamsProvider()
    {
        return array(
            'single param'       => array(
                'expected'   => array(
                    'userID' => '12345'
                ),
                'get params' => array(
                    array(
                        'key'   => 'userID',
                        'value' => '12345'
                    ),
                    array(
                        'key'   => 'random',
                        'value' => '903'
                    )
                )
            ),
            'both params set'    => array(
                'expected'   => array(
                    'userID' => '12345',
                    'sscid'  => '908',
                ),
                'get params' => array(
                    array(
                        'key'   => 'random',
                        'value' => '903'
                    ),
                    array(
                        'key'   => 'sscid',
                        'value' => '908'
                    ),
                    array(
                        'key'   => 'userID',
                        'value' => '12345'
                    )

                )
            ),
            'no params set'      => array(
                'expected'   => array(),
                'get params' => array(
                    array(
                        'key'   => 'random',
                        'value' => '908'
                    )
                )
            ),
            'empty value passed' => array(
                'expected'   => array(),
                'get params' => array(
                    array(
                        'key'   => 'userID',
                        'value' => ''
                    )
                )
            )
        );
    }
}
