<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_ImportExport
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Export model
 *
 * @category    Mage
 * @package     Mage_ImportExport
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class SM_Ximport_Model_Xexport extends Mage_ImportExport_Model_Abstract {

    protected $_fileHandler;
    protected $_delimiter = ",";
    //protected $_enclosure = '^';
    protected $_enclosure = '"';
    protected $_destination;

    protected function _construct() {

        $destination = Mage::getBaseDir() . "/var/importexport/floral.csv";

        $pathinfo = pathinfo($destination);

        if (empty($pathinfo['dirname']) || !is_writable($pathinfo['dirname'])) {
            Mage::throwException(Mage::helper('importexport')->__('Destination directory is not writable'));
        }
        if (is_file($destination) && !is_writable($destination)) {
            Mage::throwException(Mage::helper('importexport')->__('Destination file is not writable'));
        }
        $this->_destination = $destination;

        $this->_init();
    }

    /**
     * Method called as last step of object instance creation. Can be overrided in child classes.
     *
     * @return Mage_ImportExport_Model_Export_Adapter_Abstract
     */
    protected function _init() {
        $this->_fileHandler = fopen($this->_destination, 'w');
        return $this;
    }

    public function export() {

        set_time_limit(0);
        $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('name')
                //->addFieldToFilter('entity_id', 56)
                //->addFieldToFilter('entity_id', array(7661,56))
                ->addAttributeToSelect('weight')
                ->addAttributeToSelect('description')
                ->addAttributeToSelect('short_description')
                ->addAttributeToSelect('maker')
                ->addAttributeToSelect('vendor')
                ->addAttributeToSelect('price')

        ;

        $collection->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left');


        $skuFilter = Mage::app()->getRequest()->getParam('sku');

        if (!empty($skuFilter)) {
            $collection->addFieldToFilter('sku', array("like" => "$skuFilter%"));
        }

        //echo $collection->getSelect()->__toString();exit;


        $this->setHeaderCols();

        foreach ($collection as $product) {

            $productData = array(
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'product_type' => $product->getTypeId(),
                'attribute_set' => $product->getAttributeSetId(),
                'associated_sku' => '',
                'price' => $product->getPrice(),
                'qty' => (float) $product->getQty(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'weight' => $product->getWeight(),
                'maker' => $product->getMaker(),
                'vendor' => $product->getVendor(),
                'custom_options' => '',
            );


            if ($product->getTypeId() == 'grouped')
                $productData['associated_sku'] = $this->getGroupedChildSkus($product);

            if ($product->getHasOptions())
                $productData['custom_options'] = $this->getCustomOptions($product);


            $this->writeRow($productData);
        }

        return $this->getContents();
    }

    function getGroupedChildSkus($product) {

        $sql = $this->__writeAdapter()->select()
                ->from(array('relation' => 'catalog_product_relation'), null)
                ->join(array('main' => 'catalog_product_entity'), 'relation.child_id = main.entity_id', 'sku')
                ->where('parent_id = ?', $product->getId())
        ;

        $result = $this->__writeAdapter()->fetchCol($sql);

        return (implode(":", $result));
    }

    private function __writeAdapter() {
        static $__adapter;
        if (NULL === $__adapter) {
            $__adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        }
        return $__adapter;
    }

    private function getCustomOptions($product) {

        $options = "";

        $product = Mage::getModel('catalog/product')->load($product->getId());



        foreach ($product->getOptions() as $o) {
            $optionType = $o->getType();

            $options .= $o->getTitle() . ':' . $o->getType() . ':' . $o->getIsRequire() . ':' . $o->getSortOrder();

            if ($optionType == 'drop_down' || $optionType == 'radio' || $optionType == 'checkbox') {
                $values = $o->getValues();

                foreach ($values as $k => $v) {
                    $options .= "|" . $v->getTitle() . ':' . $v->getPriceType() . ':' . (float) $v->getPrice() . ':' . $v->getSortOrder();
                    //            print_r($v);
                }
            }
            $options .= '^';
        }


        return trim($options, '^');

        //}
    }

    public function getContents() {
        return file_get_contents($this->_destination);
    }

    public function setHeaderCols() {

        $headerCols = array(
            'id',
            'sku',
            'name',
            'product_type',
            'attribute_set',
            'associated_sku',
            'price',
            'qty',
            'description',
            'short_description',
            'weight',
            'maker',
            'vendor',
            'custom_options'
        );

        fputcsv(
                $this->_fileHandler, $headerCols, $this->_delimiter, $this->_enclosure
        );

        return $this;
    }

    /**
     * Write row data to source file.
     *
     * @param array $rowData
     * @throws Exception
     * @return Mage_ImportExport_Model_Export_Adapter_Abstract
     */
    public function writeRow(array $rowData) {

        fputcsv(
                $this->_fileHandler, $rowData, $this->_delimiter, $this->_enclosure
        );

        return $this;
    }

    public function getFileName() {
        return 'catalog_products_' . date('Ymd_His') . '.csv';
    }

    /**
     * Close file handler on shutdown
     */
    public function destruct() {
        if (is_resource($this->_fileHandler)) {
            fclose($this->_fileHandler);
        }
    }

}
