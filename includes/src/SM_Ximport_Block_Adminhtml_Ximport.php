<?php
class SM_Ximport_Block_Adminhtml_Ximport extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_ximport';
    $this->_blockGroup = 'ximport';
    $this->_headerText = Mage::helper('ximport')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('ximport')->__('Add Item');
    parent::__construct();
  }
}