<?php

class SM_Ximport_Block_Adminhtml_Ximport_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('ximport_form', array('legend' => Mage::helper('ximport')->__('Browse your XML file')));



        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => Mage::helper('ximport')->__('Store View'),
                'title' => Mage::helper('ximport')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        } else {

            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }

        $fieldset->addField('filename', 'file', array(
            'label' => Mage::helper('ximport')->__('Select CSV File To Import'),
            'required' => true,
            'name' => 'import_source',
        ));

        $fieldset->addField('import_options', 'checkbox', array(
            'label' => Mage::helper('ximport')->__('Import Custom Options?'),
            'required' => false,
            'name' => 'import_options',
            'onclick' => 'this.value = this.checked ? 1 : 0;',
        ));

        if (Mage::getSingleton('adminhtml/session')->getXimportData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getXimportData());
            Mage::getSingleton('adminhtml/session')->setXimportData(null);
        } elseif (Mage::registry('ximport_data')) {
            $form->setValues(Mage::registry('ximport_data')->getData());
        }
        return parent::_prepareForm();
    }

}
