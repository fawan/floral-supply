<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */
class Shopgate_Framework_Model_Modules_Affiliate_Packages_KProject_Redeem
{
    /**
     * Sets up affiliate data in session & disable place_order_after observer call
     *
     * @param Varien_Object $params - array(
     *                              'quote' => Mage Quote,
     *                              'parameters' => valid get params,
     *                              'customer_id' => customer id this quote belongs to
     *                              )
     *
     * @return bool
     */
    public function setAffiliateData(Varien_Object $params)
    {
        $parameters = $params->getData(Shopgate_Framework_Model_Modules_Affiliate_Factory::PARAMS);
        Mage::getSingleton('kproject_sas/session')->setParameters($parameters);
        Mage::register('kproject_sas_observer_disable', true, true);

        return true;
    }

    /**
     * Send transaction to ShareASale, only called from addOrder
     *
     * @param Varien_Object $params - array(
     *                              'sg_order' => Shopgate Order,
     *                              'mage_order' => Magento Order,
     *                              )
     *
     * @return Mage_Sales_Model_Order
     */
    public function promptCommission(Varien_Object $params)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $params->getData(Shopgate_Framework_Model_Modules_Affiliate_Factory::MAGE_ORDER);

        if (Mage::helper('kproject_sas')->newTransactionViaApiEnabled()) {
            $response = Mage::helper('kproject_sas/transaction')->create($order);
            Mage::helper('kproject_sas/status')->setKOrderStatus(
                $order,
                KProject_ShareASale_Helper_Status::STATUS_SUCCESS,
                $response
            );
        }

        return $order;
    }

    /**
     * Destroy cookies when done
     */
    public function destroyCookies()
    {
        Mage::getSingleton('kproject_sas/session')->unsetParameters();
        Mage::unregister('kproject_sas_observer_disable');
    }
}
