<?php

ini_set('memory_limit', '256M');
set_time_limit(0);

// 
// language mixups
//flat product table
//
class SM_Ximport_Model_Category extends Mage_Core_Model_Abstract {

    protected $__attributeCategoryId;
    protected $__entityCategoryId;
    protected $__storeIds = array();
    protected $__entityTypeId;
    protected $__attributeSetId;
    protected $__manufacturerId;
    protected $__productID;
    protected $__importOptions;

    public function _construct() {
        parent::_construct();
    }

    public function parse() {
        // get store_id from code
        $query = "SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'catalog_product'";
        $this->__entityTypeId = (int) $this->__readAdapter()->fetchOne($query);

        $query = "SELECT attribute_set_id FROM eav_attribute_set WHERE entity_type_id = '" . $this->__entityTypeId . "'  and attribute_set_name='Default'";
        $this->__attributeSetId = (int) $this->__readAdapter()->fetchOne($query);

        $query = "SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'catalog_category'";
        $this->__entityCategoryId = (int) $this->__readAdapter()->fetchOne($query);

        $query = "SELECT attribute_set_id FROM eav_attribute_set WHERE entity_type_id = '" . $this->__entityCategoryId . "'  and attribute_set_name='Default'";
        $this->__attributeCategoryId = (int) $this->__readAdapter()->fetchOne($query);

        $query = "SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'manufacturer'";
        $query = "SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'manufacturer'";
        $this->__manufacturerId = (int) $this->__readAdapter()->fetchOne($query);
    }

    public function setStoreId($storeIds) {
        $this->__storeIds = $storeIds;
    }

    public function setImportCustomOptions($importOptions) {
        $this->__importOptions = (boolean) $importOptions;
    }

    public function importCategory($categories = array(), $parentId = 1) {


        $this->parse();
        for ($i = 0; $i < count($categories); $i++) {
            //for($i=0;$i<150;$i++)
            $category = null;
            $category = $categories[$i];
            $this->insertCategory($category);
            //print_r($category); 
            //echo "<hr>"; 
        }
        //die();//if($i>20) { die('test 20 categories');}
    }

    public function importProduct($products = array()) {
        $this->parse();
        $counter = 0;
        //echo '<pre>';
        for ($i = 0; $i < count($products); $i++) {

            $product = $products[$i];
            /* 	@productCategory
             *  @update category paths/level/parent here ...
             */
            //print_r($product); 
            //echo '<hr>' . $i . '<br>';;
            //if(1 != 1 ) {
            if ($product['sku'] != "NULL") { // 783	
                if ($this->__isProductExisting($product['sku'])) { //sku	
                    // echo '<hr>updating : ' . $i . ' : ' . $product['sku'];
                    //print_r($product);	exit;			
                    $this->__updateProduct($product);
                } else {

                    //echo '<hr>inserting : ' . $i . ' : ' . $product['sku']; //  . ' | ' . $product['CATEGORY_ID_SUB1'] . ' |  ' . $product['CATEGORY_ID_ROOT'] . '<br>';
                    //print_r($product);				
                    $this->__importProduct($product);
                    //	$counter++;
                    //	echo $counter;
                    //die('inserted finished ... ');
                }
            }
        }
    }

    public function importCategoryProduct($categoryProducts = array()) {
        $this->parse();
        for ($i = 0; $i < count($categoryProducts); $i++) {
            $productCategory = $categoryProducts[$i];
            if ($this->__isProductExisting($productCategory['ART_ID'])) {//sku
                $this->__insertProductCategory($productCategory);
            }
        }
        // update child count of category
        $this->__updateChildCountOfCategory();
    }

    private function __importProduct($data) {


        $data['min'] = 1;

        //$data['description_en'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description_en']);
        //$data['description'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description']);



        $insert = array(
            'entity_type_id' => $this->__entityTypeId,
            'attribute_set_id' => $this->__attributeSetId,
            'type_id' => 'simple',
            'sku' => $data['sku'],
            'created_at' => new Zend_Db_Expr('NOW()'),
            'updated_at' => new Zend_Db_Expr('NOW()'));

        if (!empty($data['product_type']))
            $insert['type_id'] = $data['product_type'];

        //echo '<pre>';        print_r($insert);        echo '</pre>';exit;
        $this->__writeAdapter()->insert('catalog_product_entity', $insert);
        $productID = $this->__writeAdapter()->lastInsertId();



        $this->__writeAdapter()->beginTransaction();
        try {

            $data['is_in_stock'] = 1;
            $insert = array(
                'product_id' => $productID,
                'stock_id' => '1',
                'qty' => $data['qty'],
                'is_in_stock' => $data['is_in_stock']);
            $this->__writeAdapter()->insert('cataloginventory_stock_item', $insert);
            //catalog_product_website
            $insert = array(
                'product_id' => $productID,
                'website_id' => '1');
            $this->__writeAdapter()->insert('catalog_product_website', $insert);
            //catalog_product_enabled_index            

            $store_id = 0;
            $insert = array(
                'product_id' => $productID,
                'store_id' => $store_id,
                'visibility' => '4');
            $this->__writeAdapter()->insert('catalog_product_enabled_index', $insert);


            if (!array_key_exists('price', $data)) {
                $data['price'] = 0;
            }
            if (!array_key_exists('weight', $data)) {
                $data['weight'] = 1;
            }

            $this->__insertEAV("decimal", "price", $productID, $data['price']);
            $this->__insertEAV("decimal", "weight", $productID, $data['weight']);
            $this->__insertEAV("int", "status", $productID, 1);
            $this->__insertEAV("int", "visibility", $productID, 4);
            $this->__insertEAV("int", "tax_class_id", $productID, 4);
            $this->__insertEAV("text", "description", $productID, $data['description']);
            $this->__insertEAV("text", "short_description", $productID, $data['short_description']);
            $this->__insertEAV("varchar", "name", $productID, $data['name']);
            $this->__insertEAV("varchar", "qty_min", $productID, $data['min']);
            $this->__insertEAV("varchar", "maker", $productID, $data['maker']);
            $this->__insertEAV("varchar", "vendor", $productID, $data['vendor']);
            $this->__writeAdapter()->commit();

            $this->__updateGroupChildren($productID, $data);
            $this->__processProductOptions($productID, $data);
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
            echo $e->getMessage();
        }
    }

    private function __updateProduct($data) {


        $query = "SELECT entity_id FROM catalog_product_entity WHERE sku = '" . $data['sku'] . "'";
        $productID = (int) $this->__readAdapter()->fetchOne($query);
        $this->__productId = $productID;


        $this->__writeAdapter()->beginTransaction();

        try {
            /*
              $insert = array('qty' => $data['qty']);
              $this->__writeAdapter()->update('cataloginventory_stock_item', $insert, 'product_id = ' . $productID);

              if (!array_key_exists('price', $data)) {
              $data['price'] = 0;
              } */

            // price  // qty min // order unic
            //echo '<br>' . $productID	. ' sku : '  . $data['sku'] . ' price : '  . $data['price']  . ' QtyMin/Interval : '  . $data['min_qty'] ;
            $this->__updateEAV("decimal", "price", $productID, $data['price']);
            $this->__updateEAV("decimal", "weight", $productID, $data['weight']);
            $this->__updateEAV("int", "status", $productID, 1);
            $this->__updateEAV("varchar", "name", $productID, $data['name']);
            $this->__updateEAV("varchar", "maker", $productID, $data['maker']);
            $this->__updateEAV("varchar", "vendor", $productID, $data['vendor']);
            $this->__updateEAV("varchar", "random_field", $productID, $data['name']);
            $this->__updateEAV("text", "description", $productID, $data['description']);
            $this->__updateEAV("text", "short_description", $productID, $data['short_description']);
            $this->__writeAdapter()->commit();


            $this->__updateGroupChildren($productID, $data);
            $this->__processProductOptions($productID, $data);
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
            echo $e->getMessage();
        }
    }

    public function __updateGroupChildren($productID, $data) {

        if (!empty($data['associated_sku'])) {
            $this->__writeAdapter()->beginTransaction();
            $associatedSkus = explode(":", $data['associated_sku']);
            $this->__updateEAV("varchar", "options_container", $productID, "container1");

            foreach ($associatedSkus as $associatedSku) {
                $query = "SELECT entity_id FROM catalog_product_entity WHERE sku = '" . $associatedSku . "'";
                $childID = (int) $this->__readAdapter()->fetchOne($query);

                try {
                    $insert = array(
                        'parent_id' => $productID,
                        'child_id' => $childID
                    );
                    $this->__writeAdapter()->insert('catalog_product_relation', $insert);
                } catch (Exception $e) {
                    
                }

                try {
                    $insert = array(
                        'product_id' => $productID,
                        'linked_product_id' => $childID,
                        'link_type_id' => 3
                    );
                    $this->__writeAdapter()->insert('catalog_product_link', $insert);
                } catch (Exception $e) {
                    
                }

                try {
                    $linkID = $this->__writeAdapter()->lastInsertId();
                    $insert = array(
                        'link_id' => $linkID,
                        'product_link_attribute_id' => 3,
                        'value' => 0
                    );
                    $this->__writeAdapter()->insert('catalog_product_link_attribute_decimal', $insert);
                } catch (Exception $e) {
                    
                }
            }
            $this->__writeAdapter()->commit();
            //          $_product = Mage::getModel('catalog/product')->load($productID);
            //            $indexer = Mage::getSingleton('index/indexer')->processEntityAction($_product, 'cataloginventory_stock', Mage_Index_Model_Event::TYPE_SAVE);
            //   exit;
        }
    }

    private function getOptionValues($optionInfo) {
        unset($optionInfo[0]);

        $optionValues = array();
        foreach ($optionInfo as $value) {

            $optionValue = explode(":", $value);
            $optValue = array(
                "title" => $optionValue[0],
                "price_type" => $optionValue[1],
                "price" => $optionValue[2],
                "sort_order" => 0,
            );

            if (!empty($optionValue[3]))
                $optValue['sort_order'] = $optionValue[3];

            $optionValues[] = $optValue;
        }

        return $optionValues;
    }

    public function __processProductOptions($productID, $data) {

        if (!empty($data['custom_options']) && $this->__importOptions) {

            try {
                $_options = array();

                $customOptions = explode("^", $data['custom_options']);

                foreach ($customOptions as $customOption) {

                    $optionInfo = explode("|", $customOption);
                    $option = explode(":", $optionInfo[0]);
                    $opt = array(
                        "title" => $option[0],
                        "type" => $option[1],
                        "is_require" => $option[2],
                        "sort_order" => 0,
                        "values" => '',
                    );

                    if (!empty($option[3]))
                        $opt['sort_order'] = $option[3];


                    if ($opt['type'] == 'radio' || $opt['type'] == 'drop_down' || $opt['type'] == 'checkbox')
                        $opt['values'] = $this->getOptionValues($optionInfo);

                    $_options[] = $opt;
                }

                $product = Mage::getModel('catalog/product')->load($productID);
                Mage::getSingleton('catalog/product_option')->unsetOptions();
                $this->__deleteCustomOptions($productID);
                $product->setProductOptions($_options);
                $product->setCanSaveCustomOptions(true);
                $product->save();
            } catch (Exception $e) {
                
            }
        }
    }

    public function insertProductOptions($productID, $options) {
        /*
          catalog_product_option
          catalog_product_option_price
          catalog_product_option_title
          catalog_product_option_type_price
          catalog_product_option_type_title
          catalog_product_option_type_value
         */
    }

    private function __deleteCustomOptions($productID) {
        $table = '`catalog_product_option` O JOIN `catalog_product_option_type_value` V on O.option_id=V.option_id';
        $query = "DELETE O, V FROM {$table} WHERE `product_id`=" . (int) $productID . "";
        $this->__writeAdapter()->query($query);
    }

    public function insertCategory($data) {
        $flag = $this->__categoryIdExist($data['oxid']);

        if ($flag == 0) {

            $category = Mage::getModel('catalog/category');
            //exid oxtitle	oxtitle_en	oxlongdesc	oxlongdesc_en
            if (!empty($data['oxlongdesc'])) {
                $data['description'] = $data['oxlongdesc'];
            } else {
                $data['description'] = '';
            }


            if (!empty($data['oxlongdesc_en'])) {
                $data['description_en'] = $data['oxlongdesc_en'];
            } else {
                $data['description_en'] = '';
            }




            $data['description_en'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description_en']);
            $data['description'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description']);

            //file_put_contents("c:\debug.txt", print_r($data,1), FILE_APPEND);
            /* $sql = $this->__writeAdapter()->select()
              ->from('catalog_category_entity','entity_id')
              ->where('group_id = ?',$data['PARENT_ID'])
              ;
              $parentId = $this->__writeAdapter()->fetchRow($sql); */
            //file_put_contents("c:\debug.txt", print_r($parentId,1), FILE_APPEND);
            /* $sql = $this->__writeAdapter()->select()
              ->from('catalog_category_entity', 'level')
              ->where('entity_id = ?', $parentId['entity_id']);
              $result = $this->__writeAdapter()->fetchOne($sql);
              $level = $result + 1;
             */


            $parentid = 2;


            $sql = $this->__writeAdapter()->select()
                    ->from('catalog_category_entity', 'path')
                    ->where('entity_id = ?', $parentid);
            $path = $this->__writeAdapter()->fetchOne($sql);
            $this->__writeAdapter()->beginTransaction();

            // level | parent id | path |                    
            $level = 2;

            //$path = "1/2";


            try {
                $insert = array(
                    'entity_type_id' => $this->__entityCategoryId,
                    'attribute_set_id' => $this->__attributeCategoryId,
                    'parent_id' => $parentid,
                    'ox_id' => $data['oxid'],
                    'created_at' => new Zend_Db_Expr('NOW()'),
                    'updated_at' => new Zend_Db_Expr('NOW()'),
                    'path' => $path,
                    'level' => $level
                );
                //print"<pre>"; print_r($insert); die;
                //file_put_contents("c:\debug.txt", print_r($insert,1), FILE_APPEND);
                $this->__writeAdapter()->insert('catalog_category_entity', $insert);
                $categoryID = $this->__writeAdapter()->lastInsertId();
                //file_put_contents("c:\debug.txt", print_r($categoryID.'aaaa',1), FILE_APPEND);
                $this->__updateEAVcategory("int", "is_active", $categoryID, '1');
                $this->__updateEAVcategory("int", "is_anchor", $categoryID, '1');
                $this->__updateEAVcategory("int", "include_in_menu", $categoryID, '1');
                $this->__updateEAVcategory("int", "custom_design_apply", $categoryID, '1');
                $this->__updateEAVcategory("text", "description", $categoryID, $data['description']);
                $this->__updateEAVcategory("text", "available_sort_by", $categoryID, '');
                $this->__updateEAVcategory("varchar", "name", $categoryID, $data['oxtitle']);
                $this->__updateEAVcategory("varchar", "display_mode", $categoryID, 'PRODUCTS');

                $this->__updateEAVcategory("varchar", "name", $categoryID, $data['oxtitle_en'], 2);
                $this->__updateEAVcategory("text", "description", $categoryID, $data['description_en'], 2);

                //$this->__insertEAV("varchar", "url_key", $categoryID, strtolower($data['GROUP_NAME']));
                //$this->__updateEAVcategory("varchar", "image", $categoryID, $data['MIME_SOURCE_thumbnail']);
                $attributeSet = array('path' => $path . '/' . $categoryID);

                $this->__writeAdapter()->update('catalog_category_entity', $attributeSet, 'entity_id = ' . $categoryID);
                $this->__writeAdapter()->commit();


                //echo 'inserted ' . $data['oxid']. ' category';
            } catch (Exception $e) {
                //file_put_contents("c:\debug.txt", print_r($e.'    ',1), FILE_APPEND);
                $this->__writeAdapter()->rollBack();
                echo $e->getMessage();
            }
        } else { /// updating
            if (!empty($data['oxlongdesc'])) {
                $data['description'] = $data['oxlongdesc'];
            } else {
                $data['description'] = '';
            }


            if (!empty($data['oxlongdesc_en'])) {
                $data['description_en'] = $data['oxlongdesc_en'];
            } else {
                $data['description_en'] = '';
            }

            $data['description_en'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description_en']);
            $data['description'] = str_replace('[{$oViewConf->getCurrentHomeDir()}]out/pictures/', '{{media url=""}}', $data['description']);

            //file_put_contents("c:\debug.txt", print_r($data,1), FILE_APPEND);
            $sql = $this->__writeAdapter()->select()
                    ->from('catalog_category_entity', 'entity_id')
                    ->where('ox_id = ?', $data['oxid'])
            ;
            $parentId = $this->__writeAdapter()->fetchRow($sql);
            echo $categoryID = $parentId['entity_id'];
            $this->__updateEAVcategory("text", "description", $categoryID, $data['description']);
            $this->__updateEAVcategory("varchar", "name", $categoryID, $data['oxtitle']);
            //$this->__updateEAVcategory("int", "include_in_menu", $categoryID, '1');		
            //$this->__updateEAVcategory("int", "is_active", $categoryID, '1');
            $this->__updateEAVcategory("varchar", "name", $categoryID, $data['oxtitle_en'], 2);
            $this->__updateEAVcategory("text", "description", $categoryID, $data['description_en'], 2);
            echo 'updated' . $data['oxid'] . '1 product<br>';
        }
    }

    private function __insertProductCategory($oxId, $_sku) {

        echo "<br>oxId == $oxId and product sku == $_sku";

        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'entity_id')
                ->where('ox_id = ?', $oxId)
        ;
        $categoryId = $this->__writeAdapter()->fetchOne($sql);
        $sql = $this->__writeAdapter()->select()
                ->from('catalog_product_entity', 'entity_id')
                ->where('sku = ?', $_sku)
        ;
        $productId = $this->__writeAdapter()->fetchOne($sql);

        echo "<br>prodId == $productId and categId == $categoryId</hr>";

        $this->__writeAdapter()->beginTransaction();

        try {
            $sql = $this->__writeAdapter()->select()
                    ->from("catalog_category_product")
                    ->where('category_id = ?', $categoryId)
                    ->where('product_id = ?', $productId);

            $result = $this->__writeAdapter()->fetchOne($sql);
            if (!$result) {
                $insert = array(
                    'category_id' => $categoryId,
                    'product_id' => $productId
                );
                $this->__writeAdapter()->insert('catalog_category_product', $insert);
            }
            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
            echo "<br>";
            echo $e->getMessage();
            echo "<br>";
        }
    }

    private function __moveCategory($catOxId, $catParentOxId) {

        echo $catOxId . ' ' . $catParentOxId . '<hr>';

        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'entity_id')
                ->where('ox_id = ?', $catOxId)
        ;

        echo $categoryId = $this->__writeAdapter()->fetchOne($sql);
        echo ' echoed cat id<br>';

        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'entity_id')
                ->where('ox_id = ?', $catParentOxId)
        ;
        echo $categoryParentId = $this->__writeAdapter()->fetchOne($sql);
        echo ' echoed cat parent id<br>';

        $this->__writeAdapter()->beginTransaction();


        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'path')
                ->where('entity_id = ?', $categoryParentId);
        echo $path = $this->__writeAdapter()->fetchOne($sql);
        echo ' echoed cat parent path<br>';


        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'level')
                ->where('entity_id = ?', $categoryParentId);

        echo $level = $this->__writeAdapter()->fetchOne($sql);
        echo ' echoed cat parent level <br>';

        //$this->__writeAdapter()->beginTransaction(); 
        // level | parent id | path |                    
        echo $level = (int) $level + 1;


        try {


            $this->__writeAdapter()->beginTransaction();

            //$_conn = Mage::getSingleton('core/resource')->getConnection('core_write');          	

            $path = $path . '/' . $categoryId;




            $where = array("entity_id = " . (int) $categoryId);
            //$_conn->update('catalog_category_entity', $insert, $where);
            //$this->__writeAdapter()->update('catalog_category_entity', $insert, 'entity_id = ' .$categoryId);						        
            //$this->__writeAdapter()->commit();		
            $data = array(
                "parent_id" => (int) $categoryParentId,
                "path" => (string) $path,
                "level" => (int) $level
            );

            echo '<hr><pre>';
            echo '... NOW UPDATING ...<br>';
            print_r($data);
            echo '</pre>';





            //$__category = Mage::getModel('catalog/category')->load($categoryId)->addData($data);
            //echo $__category->getName();
            //echo $__category->getUrl();
            /* $__category->setData('level', $level );
              $__category->setData('parent_id', $categoryParentId);
              $__category->setData('path', $path);
              $__category->save(); */

            try {





                $category = Mage::getModel('catalog/category')->load($categoryId);
                $category->move($categoryParentId, null);

//			$__category->setId($categoryId)->save();
                echo "<h1>Data uPdated successfully.</h1>";
            } catch (Exception $e) {
                echo "<h1>ERROR !!!</h1><hr>";
                echo $e->getMessage();
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    private function __updateChildCountOfCategory() {
        $this->__writeAdapter()->beginTransaction();
        $data = array('children_count' => '1');
        try {
            $this->__writeAdapter()
                    ->update("catalog_category_entity", $data, 'entity_id > 2 ');
            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
            echo $e->getMessage();
        }
    }

    private function __writeAdapter() {
        static $__adapter;
        if (NULL === $__adapter) {
            $__adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        }
        return $__adapter;
    }

    private function __readAdapter() {
        static $__adapter;
        if (NULL === $__adapter) {
            $__adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        }
        return $__adapter;
    }

    private function __categoryIdExist($groupId) {
        $sql = $this->__writeAdapter()->select()
                ->from('catalog_category_entity', 'entity_id')
                ->where('ox_id = ?', $groupId)
        ;

        $cateId = $this->__writeAdapter()->fetchRow($sql);
        if ($cateId)
            return true;
        return false;
    }

    private function __insertEAVcategory($dataType, $code, $entityId, $value) {
        $attributeID = $this->__findAttributeCategoryID($code);
        try {
            for ($i = 0; $i < count($this->__storeIds); $i++) {
                $store_id = $this->__storeIds[$i];
                $this->__writeAdapter()->beginTransaction();
                if ($attributeID) {
                    $data = array(
                        'entity_type_id' => $this->__entityCategoryId,
                        'attribute_id' => $attributeID,
                        'store_id' => $store_id,
                        'entity_id' => $entityId,
                        'value' => $value);
                    $this->__writeAdapter()->insert("catalog_category_entity_$dataType", $data);
                }
                $this->__writeAdapter()->commit();
            }
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    private function __insertEAVcategoryStoreId($dataType, $code, $entityId, $value, $store_id) {
        echo $value;
        $attributeID = $this->__findAttributeCategoryID($code);
        try {
            $this->__writeAdapter()->beginTransaction();
            if ($attributeID) {
                $data = array(
                    'entity_type_id' => $this->__entityCategoryId,
                    'attribute_id' => $attributeID,
                    'store_id' => $store_id,
                    'entity_id' => $entityId,
                    'value' => $value);
                $this->__writeAdapter()->insert("catalog_category_entity_$dataType", $data);
            }
            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    private function __updateEAVcategory($dataType, $code, $categoryId, $value, $storeId = 0) {
        $attributeID = $this->__findAttributeCategoryID($code);
        try {
            $this->__writeAdapter()->beginTransaction();


            $store_id = $storeId;
            $sql = $this->__writeAdapter()->select()
                    ->from("catalog_category_entity_$dataType", 'value_id')
                    ->where('`entity_id` = ?', $categoryId)
                    ->where('`store_id` = ?', $store_id)
                    ->where('`attribute_id` = ?', $attributeID)
                    ->where('`entity_type_id` = ?', $this->__entityCategoryId);

            $result = $this->__writeAdapter()->fetchOne($sql);

            if (!empty($result)) {
                $data = array('value' => $value);
                $this->__writeAdapter()->update("catalog_category_entity_" . $dataType, $data, "`value_id` ='" . $result . "'");
            } else {
                $this->__insertEAVcategoryStoreId($dataType, $code, $categoryId, $value, $store_id);
            }

            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    private function __findAttributeID($code) {
        static $__list;
        if (NULL === $__list) {
            $__list = array();
        }

        $uniqueKey = md5($code);
        if (!array_key_exists($uniqueKey, $__list)) {
            $query = "SELECT attribute_id FROM eav_attribute WHERE entity_type_id = $this->__entityTypeId AND attribute_code = '$code'";
            $__list[$uniqueKey] = $this->__readAdapter()->fetchOne($query);
        }
        return $__list[$uniqueKey];
    }

    private function __isProductExisting($code) {
        $query = "SELECT COUNT(*) FROM catalog_product_entity WHERE sku = '" . $code . "'";
        $counter = (int) $this->__readAdapter()->fetchOne($query);
        if ($counter > 0) {
            return true;
        }
        return false;
    }

    private function __findAttributeCategoryID($code) {
        static $__list;
        if (NULL === $__list) {
            $__list = array();
        }
        $query = '';
        //try{
        $uniqueKey = md5($code);
        if (!array_key_exists($uniqueKey, $__list)) {
            $query = "SELECT attribute_id FROM eav_attribute WHERE entity_type_id = $this->__entityCategoryId AND attribute_code = '$code'";
            $__list[$uniqueKey] = $this->__readAdapter()->fetchOne($query);
        }
        return $__list[$uniqueKey];
        /*
          }catch(Exception $e)
          {
          Mage::log($e->getMessage().' '.$query);
          }
         */
    }

    private function __insertDescription($dataType, $code, $entityId, $value, $storeId) {
        $attributeID = $this->__findAttributeID($code);
        if ($attributeID) {
            $data = array(
                'entity_type_id' => $this->__entityTypeId,
                'attribute_id' => $attributeID,
                'store_id' => $storeId,
                'entity_id' => $entityId,
                'value' => $value);
            $this->__writeAdapter()->insert("catalog_product_entity_$dataType", $data);
        }
    }

    private function __updateDescription($dataType, $code, $entityId, $value, $storeId) {
        $attributeID = $this->__findAttributeID($code);
        if ($attributeID) {
            $data = array('value' => $value);
            $this->__writeAdapter()
                    ->update("catalog_product_entity_$dataType", $data, 'attribute_id = ' . $attributeID . ' and entity_id = ' . $entityId . ' and store_id = ' . $storeId);
        }
    }

    private function __insertEAV($dataType, $code, $entityId, $value, $store_id = 0) {
        $attributeID = $this->__findAttributeID($code);
        try {
            $this->__writeAdapter()->beginTransaction();
            //echo count($this->__storeIds);
            //print"<pre>"; print_r($this->__storeIds); die('TEST');	                
            if ($attributeID) {
                $data = array(
                    'entity_type_id' => $this->__entityTypeId,
                    'attribute_id' => $attributeID,
                    'store_id' => $store_id,
                    'entity_id' => $entityId,
                    'value' => $value);
                $this->__writeAdapter()->insert("catalog_product_entity_$dataType", $data);
            }
            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    private function __insertEAVStoreId($dataType, $code, $entityId, $value, $store_id) {
        $attributeID = $this->__findAttributeID($code);
        try {
            $this->__writeAdapter()->beginTransaction();
            if ($attributeID) {
                $data = array(
                    'entity_type_id' => $this->__entityTypeId,
                    'attribute_id' => $attributeID,
                    'store_id' => $store_id,
                    'entity_id' => $entityId,
                    'value' => $value);
                $this->__writeAdapter()->insert("catalog_product_entity_$dataType", $data);
            }
            $this->__writeAdapter()->commit();
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    /* private function __updateEAV($dataType, $code, $productId, $value, $store_id = 0) {
      $attributeId = $attributeID = $this->__findAttributeID($code);
      try{
      $this->__writeAdapter()->beginTransaction();

      $sql = $this->__writeAdapter()->select()
      ->from("catalog_product_entity_$dataType",'value_id')
      ->where('entity_id = ?', $productId)
      ->where ('store_id = ?', $store_id)
      ->where ('attribute_id = ?',$attributeID);
      $result = $this->__writeAdapter()->fetchOne($sql);
      if(!empty($result)){
      $data = array('value' => $value);
      $this->__writeAdapter()->update("catalog_product_entity_$dataType", $data, "value_id='$result'");
      }else{
      $this->__insertEAVStoreId($dataType, $code, $productId, $value,$store_id);
      }
      $this->__writeAdapter()->commit();

      }catch(Exception $e){
      $this->__writeAdapter()->rollBack();
      }
      } */

    private function __updateEAV($dataType, $code, $productId, $value) {
        $attributeId = $attributeID = $this->__findAttributeID($code);
        try {
            $this->__writeAdapter()->beginTransaction();
            for ($i = 0; $i < count($this->__storeIds); $i++) {
                $store_id = $this->__storeIds[$i];
                $sql = $this->__writeAdapter()->select()
                        ->from("catalog_product_entity_$dataType", 'value_id')
                        ->where('entity_id = ?', $productId)
                        ->where('store_id = ?', $store_id)
                        ->where('attribute_id = ?', $attributeID);
                $result = $this->__writeAdapter()->fetchOne($sql);
                if (!empty($result)) {
                    $data = array('value' => $value);
                    $this->__writeAdapter()->update("catalog_product_entity_$dataType", $data, "value_id='$result'");
                } else {
                    $this->__insertEAVStoreId($dataType, $code, $productId, $value, $store_id);
                }
                $this->__writeAdapter()->commit();
            }
        } catch (Exception $e) {
            $this->__writeAdapter()->rollBack();
        }
    }

    public function addProductAttribute($pid) {
        $data = array(
            'entity_type_id' => 4,
            'attribute_id' => 85,
            'entity_id' => $pid,
            'store_id' => 0,
            'value' => 4
        );
        $this->__writeAdapter()->insert("catalog_product_entity_int", $data);
    }

    public function insertManufacturer($optionValue, $manufactureAttributeId) {
        //insert into table eav_attribute_option
        $dataOption = array(
            'attribute_id' => $manufactureAttributeId,
        );
        $this->__writeAdapter()->insert('eav_attribute_option', $dataOption);
        $optionId = $this->__writeAdapter()->lastInsertId();
        //insert into table eav_attribute_option_value
        $dataOptionValue = array(
            'option_id' => $optionId,
            'store_id' => 0,
            'value' => $optionValue,
        );
        $this->__writeAdapter()->insert('eav_attribute_option_value', $dataOptionValue);
    }

    public function manufactureExist($optionValue) {
        $sql = $this->__writeAdapter()->select()->from('eav_attribute_option_value')
                ->where('value = ?', $optionValue)
        ;
        $result = $this->__writeAdapter()->fetchOne($sql);
        if (!empty($result))
            return true;
        return false;
    }

    public function getOptionValueManufacture($optionValue, $attributeId) {
        $sql = $this->__writeAdapter()->select()
                ->from(array('option' => 'eav_attribute_option'))
                ->join(array('option_value' => 'eav_attribute_option_value'), 'option.option_id = option_value.option_id')
                ->where('value = ?', $optionValue)
                ->where('attribute_id = ?', $attributeId)
        ;
        $result = $this->__writeAdapter()->fetchOne($sql);
        return $result;
    }

}
