<?php

class SM_Ximport_Model_Mysql4_Ximport_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ximport/ximport');
    }
}