<?php

class SM_Ximport_Block_Adminhtml_Ximport_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'ximport';
        $this->_controller = 'adminhtml_ximport';
        
        $this->_updateButton('save', 'label', Mage::helper('ximport')->__('Import Products'));
        $this->_updateButton('delete', 'label', Mage::helper('ximport')->__('Delete Item'));
		
        

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('ximport_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'ximport_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'ximport_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('ximport_data') && Mage::registry('ximport_data')->getId() ) {
            return Mage::helper('ximport')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('ximport_data')->getTitle()));
        } else {
            return Mage::helper('ximport')->__('Import products');
        }
    }
}