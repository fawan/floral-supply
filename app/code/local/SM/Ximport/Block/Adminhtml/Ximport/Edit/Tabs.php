<?php

class SM_Ximport_Block_Adminhtml_Ximport_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('ximport_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ximport')->__('Upload file'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('ximport')->__('Upload Product'),
            'title' => Mage::helper('ximport')->__('Upload file XML'),
            'content' => $this->getLayout()->createBlock('ximport/adminhtml_ximport_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
