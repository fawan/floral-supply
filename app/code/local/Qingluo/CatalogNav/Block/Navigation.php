<?php
/**
 * Qingluo.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category    Qingluo
 * @package     Qingluo_CatalogNav
 * @copyright   Copyright (c) 2010 IDVoice Inc. (http://idvoice.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Return left links and right links.
 * @category   Qingluo
 * @package    Qingluo_CatalogNav
 * @author     Qingluo Team <qingluo@idvoice.com>
 */
class Qingluo_CatalogNav_Block_Navigation extends Mage_Catalog_Block_Navigation
{
    /**
     * Render category to html
     *
     * @param Mage_Catalog_Model_Category $category
     * @param int Nesting level number
     * @param boolean Whether ot not this item is last, affects list item class
     * @param boolean Whether ot not this item is first, affects list item class
     * @param boolean Whether ot not this item is outermost, affects list item class
     * @param string Extra class of outermost list items
     * @param string If specified wraps children list in div with this class
     * @param boolean Whether ot not to add on* attributes to list item
     * @return string
     */
    protected function _renderCategoryMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false,
        $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false,
        $liNum = 0)
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();

        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);

        // prepare list item html classes
        $classes = array();
        $classes[] = 'level' . $level;
    
        $classes[] = 'nav-' . $this->_getItemPosition($level);
        $linkClass = '';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass = ' class="'.$outermostItemClass.'"';
        }
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        if ($isFirst) {
            $classes[] = 'first';
        }
        if ($isLast) {
            $classes[] = 'last';
        }
        if ($hasActiveChildren) {
            $classes[] = 'parent';
        }

        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
        if ($level != 0) {
            $attributes['style'] = 'z-index:' . (30 - $liNum); 
        }
        
        if ($hasActiveChildren && !$noEventAttributes) {
             $attributes['onmouseover'] = 'toggleMenu(this,1)';
             $attributes['onmouseout'] = 'toggleMenu(this,0)';
        }

        // assemble list item with attributes
        $htmlLi = '<li';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;
        if ($level == 0) {
                $html[] = '<div class="parent-div">';
        }
        $html[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = '<span class="product-count"> (' . $this->_getProductCount($category) . ')</span>';;
        $html[] = '</a>';
        if ($level > 0) {
            $html[] = '<div class="mini-zone"></div>';
        }

        // render children
        $htmlChildren = '';
        $j = 0;
        foreach ($activeChildren as $child) {
            $htmlChildren .= $this->_renderCategoryMenuItemHtml(
                $child,
                ($level + 1),
                ($j == $activeChildrenCount - 1),
                ($j == 0),
                false,
                $outermostItemClass,
                $childrenWrapClass,
                $noEventAttributes,
                $j
            );
            $j++;
        }
        if (!empty($htmlChildren)) {
            if ($childrenWrapClass) {
                $html[] = '<div class="' . $childrenWrapClass . '">';
            }
            if ($level > 0) {
                $html[] = '<div>';
            }
            $html[] = '<ul class="level' . $level . '">';
            $html[] = $htmlChildren;
            $html[] = '</ul>';
            if ($level > 0) {
                $html[] = '</div>';
            }
            if ($childrenWrapClass) {
                $html[] = '</div>';
            }
        }

        if ($level == 0) {
                $html[] = '</div>';
        }
        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;
    }
    
    protected function _getProductCount($category)
	{
		if (null === ($count = $category->getData('product_count')))
		{
			$count = 0;
			if ($category instanceof Mage_Catalog_Model_Category)
			{
				$count =  $category->getProductCount();
			}
			elseif ($category instanceof Varien_Data_Tree_Node)
			{
				$count = $this->_getProductCountFromTreeNode($category);
			}
		}
		return $count;
	}

	protected function _getProductCountFromTreeNode(Varien_Data_Tree_Node $category)
	{
		return Mage::getModel('catalog/category')->setId($category->getId())->getProductCount();
	}
}
