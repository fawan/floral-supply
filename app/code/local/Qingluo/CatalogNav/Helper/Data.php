<?php
/**
 * Qingluo.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * @category    Qingluo
 * @package     Qingluo_CatalogNav
 * @copyright   Copyright (c) 2010 IDVoice Inc. (http://idvoice.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Default utils.
 * @category   Qingluo
 * @package    Qingluo_CatalogNav
 * @author     Qingluo Team <qingluo@idvoice.com>
 */
class Qingluo_CatalogNav_Helper_Data extends Mage_Core_Helper_Data
{
	
}