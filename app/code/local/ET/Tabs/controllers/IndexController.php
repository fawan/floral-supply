<?php
/**
 * @package ET_Tabs
 * @version 1.0.0
 * @copyright Copyright (c) 2014 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Tabs_IndexController extends Mage_Core_Controller_Front_Action {
	
	public function indexAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function ajaxAction() {
		$response = array();
		if ($this->getRequest()->isAjax()) {
		    $this->getResponse()->setHeader('Content-Type', 'application/json');
		   
		    $params = $this->getRequest()->getParams();
		    try{
		        if (isset($params['config']) && $params['type']){
		            $block_helper = $this->getLayout()->createBlock('tabs/list', 'byajax');
		            if ($block_helper instanceof ET_Tabs_Block_List){
		                $block_helper->configFromString($params['config']);
		                $singleTab = $block_helper->getTabInstance($params['type']);
	                    $singleTab->config->no_container = true;
	                    $singleTab->config->page = 1 + $params['page'];
	                    $response['content'] = $block_helper->getTabContent($singleTab);
	                    $response['content'] = preg_replace("#[\n\r\t]?#", '', $response['content']);
	                    $response['content'] = preg_replace('~>\s*<~', '><', $response['content']);
	                    $response['content'] = trim($response['content']);
		            }
		        }
		        
// 		        die;
// 	            $block_name = isset($params['block']) ? $params['block'] : false;
// 	            if ($block_name){
//     	            $this->loadLayout();
//     	            $block_helper = $this->getLayout()->getBlock($block_name);
//     	            if ($block_helper){
//     	                if (isset($params['type'])){
//     	                    $singleTab = $block_helper->getTabInstance($params['type']);
//     	                    $singleTab->config->no_container = true;
//     	                    $singleTab->config->page = 1 + $params['page'];
//     	                    $response['content'] = $block_helper->getTabContent($singleTab);
//     	                    $response['content'] = preg_replace("#[\n\r\t]?#", '', $response['content']);
//     	                    $response['content'] = preg_replace('~>\s*<~', '><', $response['content']);
//     	                    $response['content'] = trim($response['content']);
//     	                }
//     	            }
//     	        } else {
//     	            $response['content'] = $this->__('Cannot find block[%s]', $block_name);
//     	        }
		    } catch (Exception $e) {
		        $response['content'] = $this->__('Exception:: %s', $e->getMessage());
		    }
		    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		} else {
		    $this->_redirect('/');
		}
	}
}