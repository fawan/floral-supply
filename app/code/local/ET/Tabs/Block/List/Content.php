<?php
/**
 * @package ET_Tabs
 * @version 1.0.0
 * @copyright Copyright (c) 2014 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Tabs_Block_List_Content extends ET_Tabs_Block_List {
    
    protected $_isReady = false;
    
    /**
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    public function setCollection($collection){
        $count_sql = $collection->getSelectCountSql()->reset(Zend_Db_Select::GROUP);
        $total = $collection->getConnection()->fetchOne($count_sql);
        $this->setData('total', $total);
        $this->setData('collection', $collection);
        $this->_isReady = true;
        return $this;
    }
    
    public function isFirstPage(){
        if (!$this->_isReady) return fasle;
        
        
    }
}