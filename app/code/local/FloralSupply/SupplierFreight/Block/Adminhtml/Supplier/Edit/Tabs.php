<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin edit tabs
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplier_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('floralsupply_supplierfreight')->__('Supplier'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_supplier',
            array(
                'label'   => Mage::helper('floralsupply_supplierfreight')->__('Supplier'),
                'title'   => Mage::helper('floralsupply_supplierfreight')->__('Supplier'),
                'content' => $this->getLayout()->createBlock(
                    'floralsupply_supplierfreight/adminhtml_supplier_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_supplier',
                array(
                    'label'   => Mage::helper('floralsupply_supplierfreight')->__('Store views'),
                    'title'   => Mage::helper('floralsupply_supplierfreight')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'floralsupply_supplierfreight/adminhtml_supplier_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve supplier entity
     *
     * @access public
     * @return FloralSupply_SupplierFreight_Model_Supplier
     * @author Ultimate Module Creator
     */
    public function getSupplier()
    {
        return Mage::registry('current_supplier');
    }
}
