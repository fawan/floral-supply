<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier edit form tab
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('supplier_');
        $form->setFieldNameSuffix('supplier');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'supplier_form',
            array('legend' => Mage::helper('floralsupply_supplierfreight')->__('Supplier'))
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('floralsupply_supplierfreight')->__('Supplier Name'),
                'name'  => 'title',
                'required'  => true,
                'class' => 'required-entry',

           )
        );
        
        
        $fieldset->addField(
            'prefix',
            'text',
            array(
                'label' => Mage::helper('floralsupply_supplierfreight')->__('Prefix'),
                'name'  => 'prefix',
                'required'  => true,
                'class' => 'required-entry',

           )
        );
        
        $fieldset->addField(
            'postcode',
            'text',
            array(
                'label' => Mage::helper('floralsupply_supplierfreight')->__('Zip Code'),
                'name'  => 'postcode',
                'required'  => true,
                'class' => 'required-entry',

           )
        );
        
        
/*       $fieldset->addField(
            'cost',
            'text',
            array(
                'label' => Mage::helper('floralsupply_supplierfreight')->__('Freight Cost'),
                'name'  => 'cost',
                'required'  => true,
                'class' => 'required-entry',

           )
        );*/
        
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('floralsupply_supplierfreight')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('floralsupply_supplierfreight')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('floralsupply_supplierfreight')->__('Disabled'),
                    ),
                ),
            )
        );
        
        $formValues = Mage::registry('current_supplier')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getSupplierData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getSupplierData());
            Mage::getSingleton('adminhtml/session')->setSupplierData(null);
        } elseif (Mage::registry('current_supplier')) {
            $formValues = array_merge($formValues, Mage::registry('current_supplier')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
