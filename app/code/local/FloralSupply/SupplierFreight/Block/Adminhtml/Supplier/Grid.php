<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin grid block
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('supplierGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('floralsupply_supplierfreight/supplier')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('floralsupply_supplierfreight')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'title',
            array(
                'header'    => Mage::helper('floralsupply_supplierfreight')->__('Supplier Name'),
                'align'     => 'left',
                'index'     => 'title',
            )
        );
        
        $this->addColumn(
            'prefix',
            array(
                'header'    => Mage::helper('floralsupply_supplierfreight')->__('Prefix'),
                'align'     => 'left',
                'index'     => 'prefix',
            )
        );
        
        $this->addColumn(
            'postcode',
            array(
                'header'    => Mage::helper('floralsupply_supplierfreight')->__('Zipcode'),
                'align'     => 'left',
                'index'     => 'postcode',
            )
        );
        
        /*$this->addColumn(
            'cost',
            array(
                'header'    => Mage::helper('floralsupply_supplierfreight')->__('Freight Cost'),
                'align'     => 'left',
                'index'     => 'cost',
            )
        );*/
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('floralsupply_supplierfreight')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('floralsupply_supplierfreight')->__('Enabled'),
                    '0' => Mage::helper('floralsupply_supplierfreight')->__('Disabled'),
                )
            )
        );
        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('floralsupply_supplierfreight')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('floralsupply_supplierfreight')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('floralsupply_supplierfreight')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('floralsupply_supplierfreight')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('floralsupply_supplierfreight')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('floralsupply_supplierfreight')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('floralsupply_supplierfreight')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('floralsupply_supplierfreight')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('supplier');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('floralsupply_supplierfreight')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('floralsupply_supplierfreight')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('floralsupply_supplierfreight')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('floralsupply_supplierfreight')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('floralsupply_supplierfreight')->__('Enabled'),
                            '0' => Mage::helper('floralsupply_supplierfreight')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param FloralSupply_SupplierFreight_Model_Supplier
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param FloralSupply_SupplierFreight_Model_Resource_Supplier_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
