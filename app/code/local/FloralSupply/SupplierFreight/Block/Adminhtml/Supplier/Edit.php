<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin edit form
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Block_Adminhtml_Supplier_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'floralsupply_supplierfreight';
        $this->_controller = 'adminhtml_supplier';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('floralsupply_supplierfreight')->__('Save Supplier')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('floralsupply_supplierfreight')->__('Delete Supplier')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('floralsupply_supplierfreight')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_supplier') && Mage::registry('current_supplier')->getId()) {
            return Mage::helper('floralsupply_supplierfreight')->__(
                "Edit Supplier '%s'",
                $this->escapeHtml(Mage::registry('current_supplier')->getTitle())
            );
        } else {
            return Mage::helper('floralsupply_supplierfreight')->__('Add Supplier');
        }
    }
}
