<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin block
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Block_Adminhtml_Supplier extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_supplier';
        $this->_blockGroup         = 'floralsupply_supplierfreight';
        parent::__construct();
        $this->_headerText         = Mage::helper('floralsupply_supplierfreight')->__('Supplier');
        $this->_updateButton('add', 'label', Mage::helper('floralsupply_supplierfreight')->__('Add Supplier'));

    }
}
