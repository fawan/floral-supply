<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier admin controller
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Adminhtml_SuppliersController extends FloralSupply_SupplierFreight_Controller_Adminhtml_SupplierFreight
{
    /**
     * init the supplier
     *
     * @access protected
     * @return FloralSupply_SupplierFreight_Model_Supplier
     */
    protected function _initSupplier()
    {
        $supplierId  = (int) $this->getRequest()->getParam('id');
        $supplier    = Mage::getModel('floralsupply_supplierfreight/supplier');
        if ($supplierId) {
            $supplier->load($supplierId);
        }
        Mage::register('current_supplier', $supplier);
        return $supplier;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('floralsupply_supplierfreight')->__('Suppliers'))
             ->_title(Mage::helper('floralsupply_supplierfreight')->__('Suppliers'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit supplier - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $supplierId    = $this->getRequest()->getParam('id');
        $supplier      = $this->_initSupplier();
        if ($supplierId && !$supplier->getId()) {
            $this->_getSession()->addError(
                Mage::helper('floralsupply_supplierfreight')->__('This supplier no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getSupplierData(true);
        if (!empty($data)) {
            $supplier->setData($data);
        }
        Mage::register('supplier_data', $supplier);
        $this->loadLayout();
        $this->_title(Mage::helper('floralsupply_supplierfreight')->__('Suppliers'))
             ->_title(Mage::helper('floralsupply_supplierfreight')->__('Suppliers'));
        if ($supplier->getId()) {
            $this->_title($supplier->getTitle());
        } else {
            $this->_title(Mage::helper('floralsupply_supplierfreight')->__('Add supplier'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new supplier action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save supplier - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('supplier')) {
            try {
                $supplier = $this->_initSupplier();
                $supplier->addData($data);
                $supplier->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('floralsupply_supplierfreight')->__('Supplier was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $supplier->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setSupplierData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('floralsupply_supplierfreight')->__('There was a problem saving the supplier.')
                );
                Mage::getSingleton('adminhtml/session')->setSupplierData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('floralsupply_supplierfreight')->__('Unable to find supplier to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete supplier - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $supplier = Mage::getModel('floralsupply_supplierfreight/supplier');
                $supplier->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('floralsupply_supplierfreight')->__('Supplier was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('floralsupply_supplierfreight')->__('There was an error deleting supplier.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('floralsupply_supplierfreight')->__('Could not find supplier to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete supplier - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $supplierIds = $this->getRequest()->getParam('supplier');
        if (!is_array($supplierIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('floralsupply_supplierfreight')->__('Please select suppliers to delete.')
            );
        } else {
            try {
                foreach ($supplierIds as $supplierId) {
                    $supplier = Mage::getModel('floralsupply_supplierfreight/supplier');
                    $supplier->setId($supplierId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('floralsupply_supplierfreight')->__('Total of %d suppliers were successfully deleted.', count($supplierIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('floralsupply_supplierfreight')->__('There was an error deleting suppliers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $supplierIds = $this->getRequest()->getParam('supplier');
        if (!is_array($supplierIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('floralsupply_supplierfreight')->__('Please select suppliers.')
            );
        } else {
            try {
                foreach ($supplierIds as $supplierId) {
                $supplier = Mage::getSingleton('floralsupply_supplierfreight/supplier')->load($supplierId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d suppliers were successfully updated.', count($supplierIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('floralsupply_supplierfreight')->__('There was an error updating suppliers.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'supplier.csv';
        $content    = $this->getLayout()->createBlock('floralsupply_supplierfreight/adminhtml_supplier_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'supplier.xls';
        $content    = $this->getLayout()->createBlock('floralsupply_supplierfreight/adminhtml_supplier_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'supplier.xml';
        $content    = $this->getLayout()->createBlock('floralsupply_supplierfreight/adminhtml_supplier_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/floralsupply_supplierfreight/supplier');
    }
}
