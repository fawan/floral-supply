<?php

/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * SupplierFreight module install script
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
        ->newTable($this->getTable('floralsupply_supplierfreight/supplier'))
        ->addColumn(
                'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Supplier ID'
        )->addColumn(
                'prefix', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Prefix'
        )
        ->addColumn(
                'title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Supplier Name'
        )->addColumn(
                'postcode', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Zip code'
        )->addColumn(
                'cost', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(), 'Freight Cost'
        )
        ->addColumn(
                'status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(), 'Enabled'
        )
        ->addColumn(
                'updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Supplier Modification Time'
        )
        ->addColumn(
                'created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Supplier Creation Time'
        )
        ->setComment('Supplier Table');
$this->getConnection()->createTable($table);

$this->endSetup();
