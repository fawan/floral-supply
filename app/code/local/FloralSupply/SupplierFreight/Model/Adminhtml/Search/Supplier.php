<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Model_Adminhtml_Search_Supplier extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return FloralSupply_SupplierFreight_Model_Adminhtml_Search_Supplier
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('floralsupply_supplierfreight/supplier_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $supplier) {
            $arr[] = array(
                'id'          => 'supplier/1/'.$supplier->getId(),
                'type'        => Mage::helper('floralsupply_supplierfreight')->__('Supplier'),
                'name'        => $supplier->getTitle(),
                'description' => $supplier->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/supplierfreight_supplier/edit',
                    array('id'=>$supplier->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
