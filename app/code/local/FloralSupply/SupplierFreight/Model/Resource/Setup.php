<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * SupplierFreight setup
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
}
