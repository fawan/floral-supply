<?php
/**
 * FloralSupply_SupplierFreight extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       FloralSupply
 * @package        FloralSupply_SupplierFreight
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Supplier resource model
 *
 * @category    FloralSupply
 * @package     FloralSupply_SupplierFreight
 * @author      Ultimate Module Creator
 */
class FloralSupply_SupplierFreight_Model_Resource_Supplier extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        $this->_init('floralsupply_supplierfreight/supplier', 'entity_id');
    }

   
    /**
     * Perform operations after object load
     *
     * @access public
     * @param Mage_Core_Model_Abstract $object
     * @return FloralSupply_SupplierFreight_Model_Resource_Supplier
     * @author Ultimate Module Creator
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {        
        return parent::_afterLoad($object);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param FloralSupply_SupplierFreight_Model_Supplier $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        if ($object->getStoreId()) {
            $storeIds = array(Mage_Core_Model_App::ADMIN_STORE_ID, (int)$object->getStoreId());
            $select->join(
                array('supplierfreight_supplier_store' => $this->getTable('floralsupply_supplierfreight/supplier_store')),
                $this->getMainTable() . '.entity_id = supplierfreight_supplier_store.supplier_id',
                array()
            )
            ->where('supplierfreight_supplier_store.store_id IN (?)', $storeIds)
            ->order('supplierfreight_supplier_store.store_id DESC')
            ->limit(1);
        }
        return $select;
    }

    /**
     * Assign supplier to store views
     *
     * @access protected
     * @param Mage_Core_Model_Abstract $object
     * @return FloralSupply_SupplierFreight_Model_Resource_Supplier
     * @author Ultimate Module Creator
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        
        return parent::_afterSave($object);
    }
}
