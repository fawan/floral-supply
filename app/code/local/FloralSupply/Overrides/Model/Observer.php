<?php

class FloralSupply_Overrides_Model_Observer {
    public function catalog_category_save_before($observer) {
        $category = $observer->getCategory();
        $category->setIsAnchor(0); 
        return $this;
    }
}
