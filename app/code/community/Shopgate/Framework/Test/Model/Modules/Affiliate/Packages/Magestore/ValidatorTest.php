<?php
/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */

/**
 * Testing Magestore parameter grabber.
 * Should retrieve the default "acc" key if nothing
 * is set in the system > configuration
 *
 * @coversDefaultClass Shopgate_Framework_Model_Modules_Affiliate_Packages_Magestore_Validator
 * @group              Shopgate_Modules_Affiliate
 */
class Shopgate_Framework_Test_Model_Modules_Affiliate_Packages_Magestore_ValidatorTest
    extends Shopgate_Framework_Test_Model_Utility
{

    public function setUp()
    {
        $this->getModuleConfig(Shopgate_Framework_Model_Modules_Affiliate_Packages_Magestore_Validator::MODULE_CONFIG);
    }

    /**
     * @param string $expected - class name returned from method call result
     * @param array  $config   - sets system > config custom KEY to be used
     * @param array  $params
     * @covers ::getValidParams
     *
     * @dataProvider validatorDataProvider
     */
    public function testGetValidParams($expected, $config, $params)
    {
        Mage::app()->getStore(0)->setConfig('affiliateplus/refer/url_param_array', $config);
        $class  = Mage::getModel('shopgate/modules_affiliate_packages_magestore_validator', array($params));
        $return = $class->getValidParams();

        $this->assertEquals($expected, $return);
    }

    /**
     * @return array
     */
    public function validatorDataProvider()
    {
        return array(
            'default value used if config empty'   => array(
                'expected'     => array('acc' => '1234'),
                'config value' => '',
                'get params'   => array(
                    array(
                        'key'   => 'acc',
                        'value' => '1234'
                    )
                )
            ),
            'rewrites the default value to new'    => array(
                'expected'     => array('account' => '012'),
                'config value' => ',acc,account',
                'get params'   => array(
                    array(
                        'key'   => 'account',
                        'value' => '012'
                    )
                )
            ),
            'a few values in config, last matters' => array(
                'expected'     => array('test' => '567'),
                'config value' => ',acc,account,test',
                'get params'   => array(
                    array(
                        'key'   => 'test',
                        'value' => '567'
                    )
                )
            ),
            'return if value is empty'             => array(
                'expected'     => array(),
                'config value' => ',acc,account',
                'get params'   => array(
                    array(
                        'key'   => 'account',
                        'value' => ''
                    )
                )
            )
        );
    }

    /**
     * Runs check genericChecker through some scenarios
     *
     * @param string $expected - class name returned from method call result
     * @param array  $params   - array(array('key' => 'get key', 'value' => 'get value'))
     *
     * @covers       ::checkGenericValid
     *
     * @dataProvider checkGenericValidProvider
     */
    public function testCheckGenericValid($expected, $params)
    {
        $class = Mage::getModel('shopgate/modules_affiliate_packages_magestore_validator', array($params));
        $valid = $class->checkGenericValid();

        $this->assertEquals($expected, $valid);
    }

    /**
     * @return array
     */
    public function checkGenericValidProvider()
    {
        return array(
            'correct param in get'          => array(
                'expected' => true,
                'params'   => array(
                    array(
                        'key'   => 'acc',
                        'value' => '1234'
                    )
                )
            ),
            'multiple params, 1 correct'    => array(
                'expected' => true,
                'params'   => array(
                    array(
                        'key'   => 'random',
                        'value' => '1234'
                    ),
                    array(
                        'key'   => 'acc',
                        'value' => '1234'
                    )
                )
            ),
            'incorrect param in get'        => array(
                'expected' => false,
                'params'   => array(
                    array(
                        'key'   => 'account',
                        'value' => '1234'
                    )
                )
            ),
            'correct param but empty value' => array(
                'expected' => false,
                'params'   => array(
                    array(
                        'key'   => 'acc',
                        'value' => null
                    )
                )
            ),
        );
    }

    /**
     * @after
     */
    public function after()
    {
        Mage::app()->getStore(0)->setConfig('affiliateplus/refer/url_param_array', '');
    }
}
