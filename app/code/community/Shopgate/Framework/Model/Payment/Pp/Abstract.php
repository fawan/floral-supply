<?php
/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */

/**
 * Class Shopgate_Framework_Model_Payment_Pp_Abstract
 *
 * @author  Konstantin Kiritsenko <konstantin@kiritsenko.com>
 */
class Shopgate_Framework_Model_Payment_Pp_Abstract
    extends Shopgate_Framework_Model_Payment_Abstract
    implements Shopgate_Framework_Model_Payment_Interface
{
    const PAYMENT_IDENTIFIER = 'PP';
    const MODULE_CONFIG      = 'Mage_Paypal';
    const TYPE_ORDER         = 'order';
    const TYPE_QUOTE         = 'quote';
    const TYPE_TRANS_ID      = 'transaction_id';

    /**
     * Depends on Shopgate paymentInfos() to be passed
     * into the TransactionAdditionalInfo of $order.
     *
     * @param null | string          $paymentStatus
     * @param Mage_Sales_Model_Order $order
     *
     * @return Mage_Sales_Model_Order
     */
    public function orderStatusManager(Mage_Sales_Model_Order $order, $paymentStatus = null)
    {
        $this->_getPaymentHelper()->orderStatusManager($order, $paymentStatus);
        $order->setShopgateStatusSet(true);

        return $order;
    }

    /**
     * @inheritdoc
     */
    public function prepareQuote($quote, $data)
    {
        $this->_getPaymentHelper()->importToPayment(
            $data,
            $quote->getPayment()->getMethodInstance()->getInfoInstance()
        );
        $quote->getPayment()->setTransactionId($data[self::TYPE_TRANS_ID]);
        $quote->getPayment()->setLastTransId($data[self::TYPE_TRANS_ID]);
        $quote->getPayment()->setPaypalPayerId($data['payer_id']);
        $quote->getPayment()->setPaypalPayerStatus($data['payer_status']);
        $quote->getPayment()->setAdditionalInformation('paypal_address_status', $data['address_status']);
        $quote->getPayment()->setAdditionalInformation('paypal_express_checkout_payer_id', $data[self::TYPE_TRANS_ID]);
        $quote->getPayment()->setAdditionalInformation('paypal_pending_reason', $data['pending_reason']);

        return $quote;
    }

    /**
     * @return Shopgate_Framework_Helper_Payment_Wspp
     */
    protected function _getPaymentHelper()
    {
        return Mage::helper('shopgate/payment_wspp');
    }

    /**
     * Helps retrieve IPN data from the payment info
     *
     * @param string|array $ipnData - IPN data that can come as an array or JSON
     *
     * @return array
     */
    protected function translateIpnData($ipnData)
    {
        if (empty($ipnData)) {
            $ipnData = array();
        } elseif (is_string($ipnData) && strpos($ipnData, '{') !== false) {
            $ipnData = Zend_Json::decode($ipnData);
        } elseif (is_string($ipnData)) {
            $ipnData = array($ipnData);
        }

        return $ipnData;
    }
}
