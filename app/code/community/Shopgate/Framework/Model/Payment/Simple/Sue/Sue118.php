<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */

/**
 * Handler for SUE versions 1.1.8 - 2.1.1
 */
class Shopgate_Framework_Model_Payment_Simple_Sue_Sue118
    extends Shopgate_Framework_Model_Payment_Simple_Sue_Sue211
    implements Shopgate_Framework_Model_Payment_Interface
{

    /**
     * @param Mage_Sales_Model_Order $magentoOrder
     */
    protected function setTransactionId(Mage_Sales_Model_Order $magentoOrder)
    {
        $payment = $this->getShopgateOrder()->getPaymentInfos();
        $info    = $magentoOrder->getPayment();
        if ($info instanceof Mage_Payment_Model_Info && isset($payment['transaction_id'])) {
            $info->setData('pn_su_transaction_id', $payment['transaction_id']);
        }
    }
}
