<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */
class Shopgate_Framework_Model_Modules_Affiliate_Packages_KProject_Validator
    extends Shopgate_Framework_Model_Modules_Affiliate_Validator
{
    const MODULE_CONFIG      = 'KProject_ShareASale';
    const XML_CONFIG_ENABLED = 'kproject_sas/general/enabled';

    protected $validParams = array(
        'userID' => false,
        'sscid'  => false,
    );

    /**
     * Rewrite of original method to accommodate custom keys
     * defined in the modules system > configuration section.
     * Since it only has one parameter, we can remove the old
     * one.
     */
    public function getValidParams()
    {
        if ($this->isModuleActive() && empty($this->scriptRan)) {
            $userIdKey = Mage::helper('kproject_sas')->getAffiliateIdentifierKey();
            if (!empty($userIdKey) && !isset($this->validParams[$userIdKey])) {
                $this->validParams[$userIdKey] = false;
                unset($this->validParams['userID']);
            }

            $clickIdKey = Mage::helper('kproject_sas')->getClickIdentifierKey();
            if (!empty($clickIdKey) && !isset($this->validParams[$clickIdKey])) {
                $this->validParams[$clickIdKey] = false;
                unset($this->validParams['sscid']);
            }
            $this->scriptRan = true;
        }

        return parent::getValidParams();
    }
}
