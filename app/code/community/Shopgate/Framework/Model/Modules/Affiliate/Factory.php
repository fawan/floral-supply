<?php

/**
 * Shopgate GmbH
 *
 * URHEBERRECHTSHINWEIS
 *
 * Dieses Plugin ist urheberrechtlich geschützt. Es darf ausschließlich von Kunden der Shopgate GmbH
 * zum Zwecke der eigenen Kommunikation zwischen dem IT-System des Kunden mit dem IT-System der
 * Shopgate GmbH über www.shopgate.com verwendet werden. Eine darüber hinausgehende Vervielfältigung, Verbreitung,
 * öffentliche Zugänglichmachung, Bearbeitung oder Weitergabe an Dritte ist nur mit unserer vorherigen
 * schriftlichen Zustimmung zulässig. Die Regelungen der §§ 69 d Abs. 2, 3 und 69 e UrhG bleiben hiervon unberührt.
 *
 * COPYRIGHT NOTICE
 *
 * This plugin is the subject of copyright protection. It is only for the use of Shopgate GmbH customers,
 * for the purpose of facilitating communication between the IT system of the customer and the IT system
 * of Shopgate GmbH via www.shopgate.com. Any reproduction, dissemination, public propagation, processing or
 * transfer to third parties is only permitted where we previously consented thereto in writing. The provisions
 * of paragraph 69 d, sub-paragraphs 2, 3 and paragraph 69, sub-paragraph e of the German Copyright Act shall remain
 * unaffected.
 *
 * @author Shopgate GmbH <interfaces@shopgate.com>
 */
class Shopgate_Framework_Model_Modules_Affiliate_Factory extends Shopgate_Framework_Model_Modules_Factory
{
    const PARAMS      = 'parameters';
    const QUOTE       = 'quote';
    const CUSTOMER_ID = 'customer_id';
    const USE_TAX     = 'use_tax_classes';
    const SG_ORDER    = 'sg_order';
    const MAGE_ORDER  = 'mage_order';

    /** @var Shopgate_Framework_Model_Modules_Affiliate_Router */
    private $router;
    /** @var array */
    private $validPackages = array();

    /**
     * @throws Exception
     */
    public function _construct()
    {
        parent::_construct();

        $route = next($this->_data);
        if (!$route instanceof Shopgate_Framework_Model_Interfaces_Modules_Router) {
            $error = Mage::helper('shopgate')->__('Incorrect class provided to: %s::_constructor()', get_class($this));
            ShopgateLogger::getInstance()->log($error);
            throw new Exception($error);
        }

        $this->router = $route;
    }

    /**
     * Runs the initial setup functionality. Usually setting
     * up parameters before the totals collector runs.
     *
     * @param Mage_Sales_Model_Quote | null $quote
     *
     * @return bool
     */
    public function setUp($quote = null)
    {
        $result = false;
        foreach ($this->getAllValidPackages() as $package) {
            $redeemer = $this->getRouter()->setDirectoryName($package)->getRedeemer();
            if ($redeemer && method_exists($redeemer, 'setAffiliateData')) {
                $data   = array(
                    self::PARAMS      => $this->getRouter()->setDirectoryName($package)->getValidator()->getValidParams(
                    ),
                    self::CUSTOMER_ID => $this->getSgOrder()->getExternalCustomerId(),
                    self::QUOTE       => $quote
                );
                $result = $redeemer->setAffiliateData(new Varien_Object($data));
                ShopgateLogger::getInstance()->log("Affiliate setUp with {$package}", ShopgateLogger::LOGTYPE_DEBUG);
                ShopgateLogger::getInstance()->log(print_r($data[self::PARAMS], true), ShopgateLogger::LOGTYPE_DEBUG);
            }
        }

        return $result;
    }

    /**
     * Retrieves a Shopgate coupons to export in check_cart call
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param bool                   $useTaxClasses
     *
     * @return ShopgateExternalCoupon[]
     */
    public function redeemCoupon(Mage_Sales_Model_Quote $quote, $useTaxClasses)
    {
        $coupons = array();
        foreach ($this->getAllValidPackages() as $package) {
            $redeemer = $this->getRouter()->setDirectoryName($package)->getRedeemer();
            if ($redeemer && method_exists($redeemer, 'retrieveCoupon')) {
                $data   = array(
                    self::PARAMS  => $this->getRouter()->setDirectoryName($package)->getValidator()->getValidParams(),
                    self::USE_TAX => $useTaxClasses,
                    self::QUOTE   => $quote
                );
                $coupon = $redeemer->retrieveCoupon(new Varien_Object($data));
                if ($coupon) {
                    $coupons[] = $coupon;
                }
                ShopgateLogger::getInstance()->log("Affiliate Coupon /w {$package}", ShopgateLogger::LOGTYPE_DEBUG);
            }
        }

        return $coupons;
    }

    /**
     * Trigger affiliate commission retrieval
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function promptCommission(Mage_Sales_Model_Order $order)
    {
        foreach ($this->getAllValidPackages() as $package) {
            $redeemer = $this->getRouter()->setDirectoryName($package)->getRedeemer();
            if ($redeemer && method_exists($redeemer, 'promptCommission')) {
                $data = array(
                    self::MAGE_ORDER => $order,
                    self::SG_ORDER   => $this->getSgOrder()
                );
                $redeemer->promptCommission(new Varien_Object($data));
                ShopgateLogger::getInstance()->log("Affiliate commission /w {$package}", ShopgateLogger::LOGTYPE_DEBUG);
            }
        }
    }

    /**
     * Retrieves allowed params that can be passed to our Merchant API
     * as GET params in the redirect
     *
     * @see Shopgate_Framework_Model_Modules_Affiliate_Router::getValidAffiliateParameterKeys
     *
     * @return array
     */
    public function getModuleTrackingParameters()
    {
        $params = array();
        foreach ($this->getAllValidPackages() as $package) {
            $rawParams = $this->getRouter()->setDirectoryName($package)->getValidator()->getValidParams();
            $keys      = array_keys($rawParams);
            $params    = array_merge($params, $keys);
        }

        return $params;
    }

    /**
     * Destroys cookies
     */
    public function destroyCookies()
    {
        foreach ($this->getAllValidPackages() as $package) {
            $redeemer = $this->getRouter()->setDirectoryName($package)->getRedeemer();
            if ($redeemer && method_exists($redeemer, 'destroyCookies')) {
                ShopgateLogger::getInstance()->log("Affiliate cookies /w {$package}", ShopgateLogger::LOGTYPE_DEBUG);
                $redeemer->destroyCookies();
            }
        }
    }

    /**
     * Returns all the valid package folder names
     *
     * @return array - e.g. array('Magestore')
     */
    public function getAllValidPackages()
    {
        if (!empty($this->validPackages)) {
            return $this->validPackages;
        }

        $iterator = new DirectoryIterator(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Packages' . DIRECTORY_SEPARATOR);
        foreach ($iterator as $location) {
            if (!$location->isDot() && $location->isDir()) {
                if ($this->getRouter()->setDirectoryName($location->getBasename())->getValidator()->isValid()) {
                    $this->validPackages[] = $location->getBasename();
                }
            }
        }

        if (empty($this->validPackages)) {
            ShopgateLogger::getInstance()->log(
                'None of the affiliate packages were valid',
                ShopgateLogger::LOGTYPE_DEBUG
            );
        }

        return $this->validPackages;
    }

    /**
     * @return Shopgate_Framework_Model_Modules_Affiliate_Router
     */
    private function getRouter()
    {
        return $this->router;
    }
}
